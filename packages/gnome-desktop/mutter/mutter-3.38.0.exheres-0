# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings meson

SUMMARY="Clutter and metacity based compositing window manager"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection
    remote-desktop [[ description = [ Support remote desktop and screen casting ] ]]
    wacom [[ description = [ Use libwacom for tablet identification ] ]]
    ( linguas: am ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia cs cy da de dz el en_CA
               en_GB eo es et eu fa fi fr fur ga gl gu ha he hi hr hu hy id ig is it ja ka kk kn ko ku
               la lt lv mai mg mk ml mn mr ms nb nds ne nl nn oc or pa pl pt pt_BR ro ru rw si sk
               sl sq sr sr@latin sv ta te tg th tk tr ug uk vi wa xh yo zh_CN zh_HK zh_TW )
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        sys-libs/wayland-protocols[>=1.19]
        virtual/pkg-config[>=0.21]
    build+run:
        core/json-glib
        dev-libs/fribidi[>=1.0.0]
        dev-libs/glib:2[>=2.61.1]
        gnome-desktop/gnome-desktop:3.0
        gnome-desktop/gsettings-desktop-schemas[>=3.37.2][gobject-introspection?]
        gnome-desktop/libgudev[>=232]
        gnome-desktop/zenity
        media-libs/libcanberra[>=0.26]
        sys-apps/dbus
        sys-apps/upower[>=0.99.0]
        sys-libs/libinput[>=1.7]
        sys-libs/egl-wayland
        sys-libs/wayland[>=1.18.0]
        x11-apps/xkeyboard-config
        x11-dri/libdrm[>=2.4.83]
        x11-dri/mesa[>=17.3]
        x11-libs/cairo[>=1.10.0]
        x11-libs/graphene:1.0[>=1.9.3][gobject-introspection?]
        x11-libs/gtk+:3[>=3.19.8][gobject-introspection?][wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXcomposite[>=0.2]
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi[>=1.6.99.1]
        x11-libs/libXinerama
        x11-libs/libXrandr[>=1.5.0]
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/libxkbcommon[X][>=0.4.3]
        x11-libs/libxkbfile
        x11-libs/pango[>=1.2.0]
        x11-libs/startup-notification[>=0.7]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
        remote-desktop? ( media/pipewire[>=0.3.0] )
        wacom? ( x11-libs/libwacom[>=0.19] )
    run:
        x11-server/xorg-server[xwayland]
    recommendation:
        gnome-desktop/gnome-themes-extra [[
            description = [ Provides default GNOME 3 theme (Adwaita) ]
        ]]
"

# Bundled clutter fails its tests
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dopengl=true
    -Dgles=true
    -Degl=true
    -Dglx=true
    -Dwayland=true
    -Dnative_backend=true
    -Degl_device=true
    -Dwayland_eglstream=true
    -Dudev=true
    -Dpango_ft2=true
    -Dstartup_notification=true
    -Dsm=true
    -Dcogl_tests=false
    -Dclutter_tests=false
    -Dcore_tests=true
    -Dtests=true
    -Dinstalled_tests=false
    -Dprofiler=false
    -Dverbose=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'remote-desktop remote_desktop'
    'wacom libwacom'
)

