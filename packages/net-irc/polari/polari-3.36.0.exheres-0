# Copyright 2013 Calvin Walton
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings freedesktop-desktop gtk-icon-cache
require meson

SUMMARY="An IRC Client for GNOME"
DESCRIPTION="
A simple Internet Relay Chat (IRC) client that is designed to integrate
seamlessly with GNOME; it features a simple and beautiful interface which
allows you to focus on your conversations.
"
HOMEPAGE="https://wiki.gnome.org/Apps/Polari"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/appstream-glib
        dev-util/desktop-file-utils
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.14]
    build+run:
        dev-libs/glib:2[>=2.43.4]
        gnome-bindings/gjs:1[>=1.57.3]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/gspell:1[gobject-introspection]
        net-im/telepathy-glib[gobject-introspection]
        net-im/telepathy-logger:0.2[gobject-introspection]
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[>=3.21.6][gobject-introspection]
        x11-libs/pango[gobject-introspection]
    run:
        net-im/telepathy-idle
        net-im/telepathy-mission-control
"

pkg_postinst() {
    gsettings_pkg_postinst
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

